# contributing

Simply follow this guide,

 - Explain what and how the following improves the existing source code.

 - Use Java Conventions, See Conventions to read what we mean.

 - Java 17 should be the default API used, Yes many features are backwards compatiable but use java 17 if it is a better solution to the problem you face.

 - If in doubt just ask at irc.bisi377.com, Connect to use using an irc client hexchat, weechat, issi or whatever you use but we do scan for attacks using BOPM and ddos protection is present.
