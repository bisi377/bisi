# Conventions

This will explain how to use java Conventions, currently the source code is a miss and doesn't follow any one thing but this is the end game for everything.

# Java Class Files
Use CamelCase,

Correct:

Client, Server, NpcHandler, Item

Incorrect:

clinet, server, NPCHandler, item,


# Java Methods

Use lowerCamelCase,

Correct:

    compareToIgnoreCase(String str)
    copyValueOf(char[] data)
    equalsIgnoreCase(String anotherString)

Incorrect:

    CopareToIgnoreCase(String str)
    CopyValueof(Char[] Data)
    equalsIgnoreCase(String Anotherstring)


# Constants

Screaming Snake Case, As some call it

Correct:

    public static final int CYCLE_TIME = 500;

Incorrect:
    public static final int cycleTime = 500;

The final modifier means it can not be changed

