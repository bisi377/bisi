import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Commands {
    @SuppressWarnings("static-access")
    public void command(String command, int a) {
        Client c = (Client) Server.playerHandler.players[a];

        if (command.startsWith("itm") && c.playerRights >= 2) {
            String[] args = command.split(" ");
            if (args.length == 3) {
                int newItemID = Integer.parseInt(args[1]);
                int newItemAmount = Integer.parseInt(args[2]);
                if ((newItemID <= 20000) && (newItemID >= 0)) {
                    c.addItem(newItemID, newItemAmount);
                } else {
                    c.sendMessage("No such item.");
                }
            } else {
                c.sendMessage("Usage: ::itm <itemID> <amount>");
            }
        } else if (command.startsWith("bank") && c.playerRights >= 2) {
            c.openUpBank();
        } else if (command.startsWith("crafting")) {
            // Add crafting items
            int[] craftingItems = { 1755, 1623, 1621, 1619, 1617, 1631 };
            for (int itemID : craftingItems) {
                c.addItem(itemID, 1);
            }
        } else if (command.startsWith("smelting")) {
            // Add smelting ores
            int[] smeltingOres = { 437, 439, 441, 443, 445, 448, 450, 452, 454 };
            for (int itemID : smeltingOres) {
                c.addItem(itemID, 10000);
            }
        } else if (command.equalsIgnoreCase("master")) {
            // Add experience to all skills
            int experience = 14910000;
            for (int i = 0; i <= 23; i++) {
                c.addSkillXP(experience, i);
            }
            c.sendMessage(c.playerName + " is now a Master.");
        } else if (command.startsWith("char")) {
            c.showInterface(3559);
        } else if (command.startsWith("tele")) {
            String[] args = command.split(" ");
            if (args.length >= 3) {
                c.teleportToX = Integer.parseInt(args[1]);
                c.teleportToY = Integer.parseInt(args[2]);
            } else {
                c.sendMessage("Usage: ::tele <x> <y>");
            }
        } else if (command.equals("coords")) {
            c.sendMessage("X: " + c.absX);
            c.sendMessage("Y: " + c.absY);
            c.sendMessage("Height: " + c.heightLevel);
        } else if (command.startsWith("start") && c.playerRights >= 2) {
            c.teleportToX = 3551;
            c.teleportToY = 9711;
        } else if (command.equals("home")) {
            c.teleportToX = 3223;
            c.teleportToY = 3218;
            c.heightLevel = 0;
        }
    }
}
