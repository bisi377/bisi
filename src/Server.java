import java.io.IOException;
import java.net.InetAddress;

public class Server implements Runnable {

	/*
	 * Integers
	 */
	public static final int CYCLE_TIME = 600;

	public static final int LISTEN_ON = 43594;

	public static int UPDATE_SECONDS = 180;

	public static int ENERGY_GAIN = 60;

	public static int MAX_CONNECTIONS = 100000;

	public static int SHUTDOWN_COUNTER = 0;

	public static boolean updateServer = false;

	public static long START_TIME;
	public static Server clientHandler = null;

	public static java.net.ServerSocket clientListener = null;

	public static boolean shutdownServer = false;
	public static boolean shutdownClientHandler;
	public static PlayerHandler playerHandler = null;
	public static Commands Commands = null;
	public static NpcHandler npcHandler = null;
	public static ItemHandler itemHandler = null;
	public static ShopHandler shopHandler = null;
	public static String[] connections = new String[MAX_CONNECTIONS];
	public static int[] connectionCount = new int[MAX_CONNECTIONS];
	public static boolean SHUTDOWN = false;

	/*
	 * shutdown is already true, We use the java API, Math.max to check using
	 * timeElapsed to verify calcTIme
	 */
	public static void calcTime() {
		if (!shutdownServer) {
			long curTime = System.currentTimeMillis();
			int timeElapsed = (int) (curTime - START_TIME) / 1000;
			UPDATE_SECONDS = Math.max(0, 180 - timeElapsed);
			shutdownServer = (UPDATE_SECONDS == 0);
		}
	}

	public static void main(String args[]) {
		// Initialize server components
		clientHandler = new Server();
		new Thread(clientHandler).start();

		playerHandler = new PlayerHandler();
		Commands = new Commands();
		npcHandler = new NpcHandler();
		itemHandler = new ItemHandler();
		shopHandler = new ShopHandler();

		int waitFails = 0;
		long totalTimeSpentProcessing = 0;
		long lastTicks = System.currentTimeMillis();
		int cycle = 0;

		while (!shutdownServer) {
			if (updateServer) {
				calcTime();
			}

			// Process game components
			playerHandler.process();
			npcHandler.process();
			itemHandler.process();
			shopHandler.process();

			// Measure time spent processing
			long timeSpent = System.currentTimeMillis() - lastTicks;
			totalTimeSpentProcessing += timeSpent;

			// Adjust for cycle time
			if (timeSpent >= CYCLE_TIME) {
				timeSpent = CYCLE_TIME;
				if (++waitFails > 100) {
					shutdownServer = true;
					Misc.println("[KERNEL]: machine is too slow to run this server!");
				}
			}

			// Sleep to maintain cycle time
			try {
				Thread.sleep(CYCLE_TIME - timeSpent);
			} catch (InterruptedException ignored) {
			}

			lastTicks = System.currentTimeMillis();
			cycle++;

			// Check for shutdown conditions
			if (SHUTDOWN && SHUTDOWN_COUNTER >= 100) {
				shutdownServer = true;
			}
			SHUTDOWN_COUNTER++;
		}

		// Clean up and shut down
		playerHandler.destruct();
		clientHandler.killServer();
		clientHandler = null;
	}

	public Server() {
	}

	public void killServer() {
		try {
			shutdownClientHandler = true;
			if (clientListener != null)
				clientListener.close();
			clientListener = null;
		} catch (java.lang.Exception __ex) {
			__ex.printStackTrace();
		}
	}

	public void run() {
		try {
			shutdownClientHandler = false;
			clientListener = new java.net.ServerSocket(LISTEN_ON, 1, null);
			InetAddress serverAddress = clientListener.getInetAddress();
			int serverPort = clientListener.getLocalPort();
			Misc.println("Starting WhiteScape Server on " + serverAddress.getHostAddress() + ":" + serverPort);

			while (true) {
				java.net.Socket s = clientListener.accept();
				s.setTcpNoDelay(true);
				String connectingHost = s.getInetAddress().getHostName();

				boolean shouldAcceptConnection = true;
				if (connectingHost.startsWith("computing") || connectingHost.startsWith("server2")) {
					Misc.println(connectingHost + ": Checking if server still is online...");
				} else {
					int found = -1;
					for (int i = 0; i < MAX_CONNECTIONS; i++) {
						if (connections[i] == connectingHost) {
							found = connectionCount[i];
							break;
						}
					}
					if (found >= 3) {
						shouldAcceptConnection = false;
					}
				}

				if (shouldAcceptConnection) {
					Misc.println("ClientHandler: Accepted from " + connectingHost + ":" + s.getPort());
					playerHandler.newPlayerClient(s, connectingHost);
				} else {
					s.close();
					Misc.println("ClientHandler: Rejected " + connectingHost + ":" + s.getPort());
				}
			}
		} catch (IOException ioe) {
			if (!shutdownClientHandler) {
				Misc.println("Error: Unable to startup listener on " + LISTEN_ON + " - port already in use?");
			} else {
				Misc.println("ClientHandler was shut down.");
			}
		}
	}
}
