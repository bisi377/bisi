import bisi.util.DateManager;

public class SkillingCheck {
    public static boolean checkObjectSkill(int objectID) {
        boolean goFalse = false;
        int xpModifier = DateManager.getXPModifier();
        switch (objectID) {
            case 1276, 1277, 1278, 1279, 1280, 1330, 1332, 2409, 3033, 3034, 3035, 3036, 3879, 3881, 3882, 3883,
                 1315, 1316, 1318, 1319, 1282, 1283, 1284, 1285, 1286, 1287, 1289, 1290, 1291, 1365, 1383, 1384,
                 5902, 5903, 5904 -> {
                Client.woodcutting[0] = 1;
                Client.woodcutting[1] = 1;
                Client.woodcutting[2] = 25 * xpModifier;
                Client.woodcutting[4] = 1511;
                break;
            }
            case 2023 -> {
                Client.woodcutting[0] = 2;
                Client.woodcutting[1] = 1;
                Client.woodcutting[2] = 25 * xpModifier;
                Client.woodcutting[4] = 2862;
                break;
            }
            case 1281, 3037 -> {
                Client.woodcutting[0] = 3;
                Client.woodcutting[1] = 15;
                Client.woodcutting[2] = 38 * xpModifier;
                Client.woodcutting[4] = 1521;
                break;
            }
            case 1308, 5551, 5552, 5553 -> {
                Client.woodcutting[0] = 4;
                Client.woodcutting[1] = 30;
                Client.woodcutting[2] = 68 * xpModifier;
                Client.woodcutting[4] = 1519;
                break;
            }
            case 9036 -> {
                Client.woodcutting[0] = 5;
                Client.woodcutting[1] = 35;
                Client.woodcutting[2] = 85 * xpModifier;
                Client.woodcutting[4] = 6333;
                break;
            }
            case 1292 -> {
                Client.woodcutting[0] = 5;
                Client.woodcutting[1] = 36;
                Client.woodcutting[2] = 0 * xpModifier;
                Client.woodcutting[4] = 771;
                break;
            }
            case 1307, 4674 -> {
                Client.woodcutting[0] = 6;
                Client.woodcutting[1] = 45;
                Client.woodcutting[2] = 100 * xpModifier;
                Client.woodcutting[4] = 1517;
                break;
            }
            case 2289, 4060 -> {
                Client.woodcutting[0] = 7;
                Client.woodcutting[1] = 45;
                Client.woodcutting[2] = 83 * xpModifier;
                Client.woodcutting[4] = 3239;
                break;
            }
            case 9034 -> {
                Client.woodcutting[0] = 8;
                Client.woodcutting[1] = 50;
                Client.woodcutting[2] = 125 * xpModifier;
                Client.woodcutting[4] = 4445;
                break;
            }
            case 1309 -> {
                Client.woodcutting[0] = 9;
                Client.woodcutting[1] = 60;
                Client.woodcutting[2] = 175 * xpModifier;
                Client.woodcutting[4] = 1515;
                Client.woodcutting[5] = 3;
                break;
            }
            case 1306 -> {
                Client.woodcutting[0] = 10;
                Client.woodcutting[1] = 75;
                Client.woodcutting[2] = 250 * xpModifier;
                Client.woodcutting[4] = 1513;
                break;
            }
            case 2491 -> {
                Client.mining[0] = 1;
                Client.mining[1] = 1;
                Client.mining[2] = 5 * xpModifier;
                Client.mining[4] = 1436;
                break;
            }
            case 2108, 2109 -> {
                Client.mining[0] = 1;
                Client.mining[1] = 1;
                Client.mining[2] = 5 * xpModifier;
                Client.mining[4] = 434;
                break;
            }
            case 2090, 2091 -> {
                Client.mining[0] = 1;
                Client.mining[1] = 1;
                Client.mining[2] = 17 * xpModifier;
                Client.mining[4] = 436;
                break;
            }
            case 2094, 2095 -> {
                Client.mining[0] = 1;
                Client.mining[1] = 1;
                Client.mining[2] = 17 * xpModifier;
                Client.mining[4] = 438;
                break;
            }
            case 2110 -> {
                Client.mining[0] = 2;
                Client.mining[1] = 10;
                Client.mining[2] = 17 * xpModifier;
                Client.mining[4] = 668;
                break;
            }
            case 4028, 4029, 4030 -> {
                Client.mining[0] = 1;
                Client.mining[1] = 1;
                Client.mining[2] = 26 * xpModifier;
                Client.mining[4] = 3211;
                break;
            }
            case 2092, 2093 -> {
                Client.mining[0] = 3;
                Client.mining[1] = 15;
                Client.mining[2] = 35 * xpModifier;
                Client.mining[4] = 440;
                break;
            }
            case 2100, 2101 -> {
                Client.mining[0] = 4;
                Client.mining[1] = 20;
                Client.mining[2] = 40 * xpModifier;
                Client.mining[4] = 442;
                break;
            }
            case 3403 -> {
                Client.mining[0] = 4;
                Client.mining[1] = 20;
                Client.mining[2] = 20 * xpModifier;
                Client.mining[4] = 2892;
                break;
            }
            case 2096, 2097 -> {
                Client.mining[0] = 5;
                Client.mining[1] = 30;
                Client.mining[2] = 50 * xpModifier;
                Client.mining[4] = 453;
                break;
            }
            case 2098, 2099 -> {
                Client.mining[0] = 6;
                Client.mining[1] = 40;
                Client.mining[2] = 65 * xpModifier;
                break;
            }
            case 2102, 2103 -> {
                Client.mining[0] = 7;
                Client.mining[1] = 55;
                Client.mining[2] = 80 * xpModifier;
                Client.mining[4] = 447;
                break;
            }
            case 2104, 2105 -> {
                Client.mining[0] = 8;
                Client.mining[1] = 70;
                Client.mining[2] = 95 * xpModifier;
                Client.mining[4] = 449;
                break;
            }
            case 2106, 2107 -> {
                Client.mining[0] = 9;
                Client.mining[1] = 85;
                Client.mining[2] = 125 * xpModifier;
                Client.mining[4] = 451;
                break;
            }
            default -> goFalse = true;
        }
        if (goFalse) {
            return false;
        }
        return true;
    }
}
