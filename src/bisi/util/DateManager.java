package bisi.util;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class DateManager {
    public static final int WEEKEND_BONUS = 10; // 10 * Normal Runescape Experience
    public static final int[] WEEKEND_DAYS = { Calendar.FRIDAY, Calendar.SATURDAY, Calendar.SUNDAY };

    /**
     * Check if today is a weekend day.
     */
    public static boolean isWeekend() {
        Calendar cal = new GregorianCalendar();
        int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
        for (int weekendDay : WEEKEND_DAYS) {
            if (dayOfWeek == weekendDay) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get the XP modifier for the current day.
     */
    public static int getXPModifier() {
        return isWeekend() ? WEEKEND_BONUS : 1;
    }
}
